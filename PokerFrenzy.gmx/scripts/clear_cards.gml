// ----------
// This script clears out all of the old card objects
// and creates new cards which only show the card backs
//
// This is used to reset the card objects in between deals
// ----------
global.bet = 0;
global.win = 0;

// Get rid of the old cards
for (var i=0; i < ds_list_size(global.main_courante); i++) 
    {
    if instance_exists(global.main_courante[| i]) 
        {
        with (global.main_courante[| i]) { instance_destroy(); }
        }
    }

// empties all the rest of the data structures

ds_list_clear(global.main_courante);
ds_grid_clear(global.ds_grid_already_picked,false);

// Create 5 card back instances ... these are used as placeholders
// to give the deal script somthing to convert into 'real' cards

/// animation pour les PH de cartes

with (img_card_PH1) 
    {
    uiSprite = spr_back_2;
    uiVisible = true;
    }
 
with (img_card_PH2) 
    {
    uiSprite = spr_back_2;
    uiVisible = true;
    }

with (img_card_PH3) 
    {
    uiSprite = spr_back_2;
    uiVisible = true
    }

with (img_card_PH4) 
    {
    uiSprite = spr_back_2;
    uiVisible = true;
    }

with (img_card_PH5) 
    {
    uiSprite = spr_back_2;
    uiVisible = true;
    }

// limites boutons credit
scr_limit_bets_buttons();;

// efface le texte de resultat de main
with (txt_hand_result) { uiTextValue = ''; }

scr_scoreboard_reset();
