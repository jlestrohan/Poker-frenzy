///scr_scoreboard_blink(ui component,effect_on)

/*
    script d'animation de composant passés en parametre pour le scoreboard
*/

var component = argument0;

if !instance_exists(component) or component.object_index != ui_label { exit; }

if global.is_draw 
    {
    with (txt_win) 
        {
        uiVisible = true;
        }
    with (lbl_win) 
        {
        uiSetScore = global.win;
        uiVisible = true;
        }
    }


// animation ?
with (component) 
    {
    uiTextColor = c_white;
    uiFading = 0.5;
    uiFadingSpeed = 1;
    uiAnimated=true;    
    uiGlowing = 1;
    uiTextWobbleSpeed = 0.2;
    uiTextWobble = 5;
    }
