// allume en rouge le panel correspondant au pari en cours

switch (global.bet) 
    {
    case 0: 
        with (pnl_table1) {uiBackColor = c_black; uiBackColor2 = c_black;}
        with (pnl_table2) {uiBackColor = c_black; uiBackColor2 = c_black;}
        with (pnl_table3) {uiBackColor = c_black; uiBackColor2 = c_black;}
        with (pnl_table4) {uiBackColor = c_black; uiBackColor2 = c_black;}
        with (pnl_table5) {uiBackColor = c_black; uiBackColor2 = c_black;}
        break;
        
    case 1: 
        with (pnl_table1) {uiBackColor = c_red; uiBackColor2 = c_red;}
        with (pnl_table2) {uiBackColor = c_black; uiBackColor2 = c_black;}
        with (pnl_table3) {uiBackColor = c_black; uiBackColor2 = c_black;}
        with (pnl_table4) {uiBackColor = c_black; uiBackColor2 = c_black;}
        with (pnl_table5) {uiBackColor = c_black; uiBackColor2 = c_black;}
        break; 
        
    case 2: 
        with (pnl_table1) {uiBackColor = c_black; uiBackColor2 = c_black;}
        with (pnl_table2) {uiBackColor = c_red; uiBackColor2 = c_red;}
        with (pnl_table3) {uiBackColor = c_black; uiBackColor2 = c_black;}
        with (pnl_table4) {uiBackColor = c_black; uiBackColor2 = c_black;}
        with (pnl_table5) {uiBackColor = c_black; uiBackColor2 = c_black;}
        break;  
        
    case 3: 
        with (pnl_table1) {uiBackColor = c_black; uiBackColor2 = c_black;}
        with (pnl_table2) {uiBackColor = c_black; uiBackColor2 = c_black;}
        with (pnl_table3) {uiBackColor = c_red; uiBackColor2 = c_red;}
        with (pnl_table4) {uiBackColor = c_black; uiBackColor2 = c_black;}
        with (pnl_table5) {uiBackColor = c_black; uiBackColor2 = c_black;}
        break; 
        
    case 4: 
        with (pnl_table1) {uiBackColor = c_black; uiBackColor2 = c_black;}
        with (pnl_table2) {uiBackColor = c_black; uiBackColor2 = c_black;}
        with (pnl_table3) {uiBackColor = c_black; uiBackColor2 = c_black;}
        with (pnl_table4) {uiBackColor = c_red; uiBackColor2 = c_red;}
        with (pnl_table5) {uiBackColor = c_black; uiBackColor2 = c_black;}
        break;
        
    case 5: 
    case 6:
        with (pnl_table1) {uiBackColor = c_black; uiBackColor2 = c_black;}
        with (pnl_table2) {uiBackColor = c_black; uiBackColor2 = c_black;}
        with (pnl_table3) {uiBackColor = c_black; uiBackColor2 = c_black;}
        with (pnl_table4) {uiBackColor = c_black; uiBackColor2 = c_black;}
        with (pnl_table5) {uiBackColor = c_red; uiBackColor2 = c_red;}
        break;
    }
