///scr_bubble(bubble_msg)

var bmsg = argument0;

var bx = (room_width-sprite_get_width(s_dialog)-(room_width-sprite_get_width(s_dialog)))/2;
var by = room_height-sprite_get_height(s_dialog);

var tmpbub = instance_create(bx+200,by,ui_bubble);
with (tmpbub) {
    depth = -800;
    uiBubbleWidth=room_width-400;
    uiBubblePos=0;
    uiTextValue=bmsg;
    uiTextColor=c_black;
    //curLB.uiBackColor2=c_black;
    curLB.uiDrawOutlines=0;
    curLB.depth = depth-1;
    uiFont=fMessageBox;
    uiBubbleSprite=s_dialog;
    uiAutoDestroy=false;
    uiBubbleMin=200;
    uiPopUp=true;
    uiDrawBackColor=true;
    canfocus=true;
}
