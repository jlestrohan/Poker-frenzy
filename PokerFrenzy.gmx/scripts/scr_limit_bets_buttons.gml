///scr_limit_bets_buttons
/*
    gere les boutons de pari en fonction des credits restant
*/

if (global.credits < 1 or global.bet >= 5) 
    {
    with (btn_bet1) { uiEnabled = false; }
    with (btn_betmax) { uiEnabled = false; }
    } 
else 
    {
    with (btn_bet1) { uiEnabled = true; }
    with (btn_betmax) { uiEnabled = true; }
    }


with (lbl_bet) { uiSetScore = global.bet; }

with (lbl_credits) { uiSetScore = global.credits; }
global.is_draw = false;
