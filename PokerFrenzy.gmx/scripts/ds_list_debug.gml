///ds_list_debug(id)

/*
    affiche le contenu de la ds_list donnée en argument DEBUG ONLY
*/
if global.release { exit; }

var list = argument0;

if (global.showDebug) 
    { 
    show_debug_message('---------------------------------'); 
    for (var i=0; i < ds_list_size(list); i++) 
        {   
        show_debug_message('POS: '+string(i)+' - value: '+string(list[| i]));
        }
    }
