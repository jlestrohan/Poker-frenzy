///scr_screenshot

/*
    takes a screenshot of the current screen to be used as alpha background
    on a new opening window
*/
if (os_browser != browser_not_a_browser) { instance_destroy(); }

var inst = instance_create(0,0,o_spinner);

var tempSurf;
tempSurf = surface_create (room_width,room_height); //create temporary surface with the size of the view
surface_set_target(tempSurf); //set the target to the surface, this will allow us to draw on it
draw_clear_alpha(c_black,0); //clear the surface

surface_copy(tempSurf, 0, 0, application_surface);
//do any other image manipulation you want

surface_reset_target(); //reset the surface target
surface_save(tempSurf,"currentSCR.png"); //save the surface
surface_free(tempSurf); //free temporary surface to free memory

with (inst) instance_destroy();
