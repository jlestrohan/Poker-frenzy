///iapFail()

/*
    This script assumes you want to do custom actions per-IAP on failed purchase attempts.
    Otherwise, you can remove this script and simply do a generic show_message_async()
    back in objIAPController's IAP event.
*/

var product = global.iapEventMap[? "product"]; // Find out which product they attempted

switch (product)
{
    case global.noAdsName: 
    {
        show_message_async("No-ads purchase failed!");
        break;
    }
    
    case "hundredcoins": 
    {
        show_message_async("100 Coins purchase failed!");
        break;
    }
    
    case "extralife": 
    {
        show_message_async("Extra Life purchase failed!");
        break;
    }
    
    default:
    {
        show_debug_message("!! Invalid iapFail product was returned!!");
        break;
    }
}
