///iapSuccess()

/*
    Here is where you put the success tasks for unlocking your content.
    Each IAP you have needs a matching case added into the switch, otherwise default will be performed.
*/

var product = global.iapEventMap[? "product"]; // Find out which product they have bought

global.saveData[? product] = true; // Mark the purchase as made, just in case something goes wrong now

// Strip the "_mac" if it's there, so we don't need to duplicate all our cases in the switch below.
var macCheckPos = string_pos("_mac", product)
if (macCheckPos >= 1) { strippedProduct = string_delete(product, macCheckPos, 4); }
else { strippedProduct = product; }


switch (strippedProduct)
{
    case global.noAdsName: 
    {
        if (global.adsEnabled)
        {
                // TODO: Replace/add the function below for your chosen ad provider
                // if you're using an ad provider on iOS/Android.
                ads_disable(0);
                global.adsEnabled = false;          // Disable ad checks throughout the game
                global.saveData[? "ads"] = false;   // Save that ads have been disabled
                
                // Our "no ads" IAP is durable, so don't set product back to false
        }
        
        break;
    }
    
    
    case "hundredcoins":
    {
        // Act on the IAP
        global.coins += 100;
        iap_consume(product);
       
        // Save the updated info
        global.saveData[? "coins"] = global.coins;
        global.saveData[? product] = false; // Set the purchase back to being "available", so they can buy it again
      
        break;
    }

    
    case "extralife":
    {
        // Act on the IAP
        global.startingLives++; 
        iap_consume(product);
            
        // Save the updated info
        global.saveData[? "lives"] = global.startingLives;
        global.saveData[? product] = false;
        
        break;
    }
    
    
    default: { show_message_async("Unknown purchase ID returned by the store! Please try again."); break;}
}

// Finally, write back the updated details
ds_map_secure_save(global.saveData, "data.dat");
