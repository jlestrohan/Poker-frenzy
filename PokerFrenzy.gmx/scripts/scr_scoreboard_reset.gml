///scr_scoreboard_reset

/*
    script de reset des effets du scoreboard
*/

var resetCol = c_yellow;

//txt
with (txt_natural_royal) 
    {
    uiTextColor = resetCol;
    uiAnimated = false;    
    }

with (txt_straight_flush) 
    {
    uiTextColor = resetCol;
    uiAnimated = false;    
    }

with (txt_four_of_a_kind) 
    {
    uiTextColor = resetCol;
    uiAnimated = false;    
    }

with (txt_full_house) 
    {
    uiTextColor = resetCol;
    uiAnimated = false;    
    }

with (txt_flush)    
    {
    uiTextColor = resetCol;
    uiAnimated = false;    
    }

with (txt_straight) 
    {
    uiTextColor = resetCol;
    uiAnimated = false;    
    }

with (txt_three_of_a_kind) 
    {
    uiTextColor = resetCol;
    uiAnimated = false;    
    }
    
with (txt_two_pairs) 
    {
    uiTextColor = resetCol;
    uiAnimated = false;    
    }

with (txt_jacks_or_better) 
    {
    uiTextColor = resetCol;
    uiAnimated = false;    
    }


with (txt_jacks_or_better) 
    {
    uiTextColor = resetCol;
    uiAnimated = false;    
    }

with (lbl_bet) { uiSetScore = global.bet; }
with (btn_draw) { uiTextValue = ''; }
