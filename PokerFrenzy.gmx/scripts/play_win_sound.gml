// This script is called when the player gets a winning hand
// It provides the sound effect

if global.sound 
    {
    if !audio_is_playing(winner_snd) { audio_play_sound(winner_snd,1,false); }
    }
