///populate_card(rank,suit,x,y)

/*
    input a card object and return it populated according to the number given as argument
    
    // SUITS
    // 0 - clubs (trefles) 
    // 1 - diamonds (carreaux)
    // 2 - hearts (coeurs)
    // 3 - spades (piques)
    
*/
rank = argument0;
suit = argument1;
var posX = argument2;
var posY = argument3;

var card_obj;

card_obj = instance_create(0,0,o_card);

sprite = 0;
switch (argument1) {
    case 0:
        sprite = asset_get_index("spr_clubs_" + string(global.cardset+1));
        break;
    case 1:
        sprite = asset_get_index("spr_diamonds_" + string(global.cardset+1));
        break;
    case 2:
        sprite = asset_get_index("spr_hearts_" + string(global.cardset+1));
        break;
    case 3:
        sprite = asset_get_index("spr_spades_" + string(global.cardset+1));
        break;
}

with (card_obj) {
    x = posX;
    y = posY;
    suit = other.suit;
    rank = other.rank;
    in_deck = true;
    image_index = other.rank-1;
    sprite_index = other.sprite;
}



return card_obj;
