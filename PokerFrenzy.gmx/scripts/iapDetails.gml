///iapDetails("Event Type")

/*
    Show debug details for an IAP event (if required).
*/

if (global.showDebug)
{
    switch (argument0)
    {  
        case "PURCHASE": 
        case "purchase":
        {
            show_debug_message("IAP PURCHASE");
            if (global.iapEventMap[? "product"] != undefined)   { show_debug_message("  Product  = " + string(global.iapEventMap[? "product"])); }
            if (global.iapEventMap[? "order"] != undefined)     { show_debug_message("  Order ID = " + string(global.iapEventMap[? "order"])); }
            if (global.iapEventMap[? "token"] != undefined)     { show_debug_message("  Token    = " + string(global.iapEventMap[? "token"])); }
            if (global.iapEventMap[? "payload"] != undefined)   { show_debug_message("  Payload  = " + string(global.iapEventMap[? "payload"])); }
            if (global.iapEventMap[? "receipt"] != undefined)   { show_debug_message("  Receipt  = " + string(global.iapEventMap[? "receipt"])); }
            if (global.iapEventMap[? "time"] != undefined)      { show_debug_message("  Time     = " + string(global.iapEventMap[? "time"])); }
            if (global.iapEventMap[? "status"] != undefined)   
            {
                switch (global.iapEventMap[? "status"])
                {
                    case iap_available: { show_debug_message("  Status   = iap_available"); break; }
                    case iap_failed:    { show_debug_message("  Status   = iap_failed"); break; }
                    case iap_purchased: { show_debug_message("  Status   = iap_purchased"); break; }
                    case iap_canceled:  { show_debug_message("  Status   = iap_canceled"); break; }
                    case iap_refunded:  { show_debug_message("  Status   = iap_refunded"); break; }
                }
            }
            if (global.iapEventMap[? "response"] != undefined)  { show_debug_message("  Response = " + string(global.iapEventMap[? "response"])); }

            break;
        }
    

        case "PRODUCT":
        case "product":
        {
            show_debug_message("IAP PRODUCT");
            if (global.iapEventMap[? "id"] != undefined)            { show_debug_message("  ID       = " + string(global.iapEventMap[? "id"])); }
            if (global.iapEventMap[? "title"] != undefined)         { show_debug_message("  Title    = " + string(global.iapEventMap[? "title"])); }
            if (global.iapEventMap[? "description"] != undefined)   { show_debug_message("  Desc     = " + string(global.iapEventMap[? "description"])); }
            if (global.iapEventMap[? "price"] != undefined)         { show_debug_message("  Price    = " + string(global.iapEventMap[? "price"])); }
            if (global.iapEventMap[? "type"] != undefined)          { show_debug_message("  Type     = " + string(global.iapEventMap[? "type"])); }
            if (global.iapEventMap[? "verified"] != undefined)      { show_debug_message("  Verified = " + string(global.iapEventMap[? "verified"])); }
            break;
        }
        
        
        default: { break; }
        
    }// End of switch (argument0)
} // End of if (global.showDebug)
