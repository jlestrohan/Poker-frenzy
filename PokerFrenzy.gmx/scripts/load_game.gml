/*
    recupere les variables globales sauvées la derniere fois
*/

var dsmap_allGlobals = ds_map_create();

global.credits = 0;
global.premium = false;
global.launches = 0;
global.totalWins = 0;

// ------------------------------------------- DEVICES / WINDOWS
if file_exists("g_data.dat") 
    {
    dsmap_allGlobals = ds_map_secure_load("g_data.dat");

    global.credits = dsmap_allGlobals[? "credits"];
   
    //nombre de jetons gagnés au cours du jeu
    global.totalWins = dsmap_allGlobals[? "totalwins"];       
    global.premium = dsmap_allGlobals[? "premium"];
    global.launches = dsmap_allGlobals[? "launches"];
    } 
else 
    {
    ds_map_add(dsmap_allGlobals, "credits", 0);
    ds_map_add(dsmap_allGlobals, "totalwins", 0);
    ds_map_add(dsmap_allGlobals, "premium", false);
    ds_map_add(dsmap_allGlobals, "launches", 0);
    global.credits = START_CREDITS; // si premier lancement et credits = 0 alors credits = 50
    }

ds_map_destroy(dsmap_allGlobals);
    
