

// on crée une array de ranks (13)
var ranks = 0;
for (var i=13; i>=1; i--) 
    {
    ranks[i] = 0; //size the array
    }
 
var dsmap_suits = ds_map_create();

ds_map_add(dsmap_suits, "clubs", 0);
ds_map_add(dsmap_suits, "spades", 0);
ds_map_add(dsmap_suits, "hearts", 0);
ds_map_add(dsmap_suits, "diamonds", 0);


// loops thru the global.main_courante ds_list to determinate what kind of game we have here
/*
For simplicity's sake, we've used card ranks starting at 1 for ace instead of 0 for ace. 
If we use 0 for ace, then we would be using 9 for 10, which is just confusing. 
Since our card ranks run 1-13, the first index of our array (0) will be empty.
*/
for (var i=0; i < ds_list_size(global.main_courante); i++) 
    {
    var inst = global.main_courante[| i];
    if instance_exists(inst) 
        {
        if (global.showDebug) { show_debug_message("card suit is: "+string(inst.suit)+ " - rank is: "+string(inst.rank)); }
        switch (inst.suit) 
            {
            case suits.clubs: dsmap_suits[? "clubs"] ++; break;
            case suits.diamonds: dsmap_suits[? "diamonds"] ++; break;
            case suits.hearts: dsmap_suits[? "hearts"] ++; break;
            case suits.spades: dsmap_suits[? "spades"] ++; break;
            } 
        ranks[inst.rank]++; // incrémente chaque carte correspondante dans l'aray des rangs
        }  
    }
    
if (global.showDebug) { show_debug_message ("GLOBAL BET: "+string(global.bet)); }
// DEBUG ----------------------------------------------------------------
//debug suits/ranks
if !global.release 
    {
    for (var i = 0; i < array_length_1d(ranks); i++) 
        {
        show_debug_message("rank: "+string(i) + " - value: "+string(ranks[i])); 
        }
    show_debug_message("spades :" + string(dsmap_suits[? "spades"]));
    show_debug_message("hearts :" + string(dsmap_suits[? "hearts"]));
    show_debug_message("diamonds :" + string(dsmap_suits[? "diamonds"]));
    show_debug_message("clubs :" + string(dsmap_suits[? "clubs"]));
    }
    
// ----------------------------------------------------------------------
/*
Okay, so now, we have our array of card ranks, now we need to find if there are actually any pairs. 
We need to know if there is a pair, and if there is, what rank the pair is. So we make an int sameCards to record 
how many cards are of the same rank, and an int groupRank to hold the rank of the pair. 
We make an int sameCards because we may have more than two cards of the same value, maybe even 3 or 4 
(hopefully not 5, unless our processor is a crooked dealer). 
We could've just made a bool isPair, but we want to know if there is a 3 or 4 of a kind as well.
*/

var sameCards=1,sameCards2=1;
var largeGroupRank=0,smallGroupRank=0;
var topStraightValue=0;
//initialze to 1int largeGroupRank=0,smallGroupRank=0;
for (var i=13; i>=1; i--) 
    {
     if (ranks[i] > sameCards)
        {
         if (sameCards != 1)  //if sameCards was not the default value
             {
             sameCards2 = sameCards;
             smallGroupRank = largeGroupRank;
             }
         
         sameCards = ranks[i];
         largeGroupRank = i;
        } 
     else 
        {
        if (ranks[i] > sameCards2)
            {
            sameCards2 = ranks[i];
            smallGroupRank = i;
            }
        }
     }
/*
sameCards starts at 1, so if we find a rank of which there are two cards, then we record 2 in sameCards 
and rank (i) as groupRank. This will work fine if there's a pair, three of a kind, or four of a kind.
But wait a second, let's say we have a full house. There is a pair of kings, so we record 2 as sameCards 
and 13 as groupRank. But we keep going through the other ranks, and if there are 3 fives, 
then we overwrite sameCards with 3 since the number of cards of that rank is more than the current value of 
sameCards. Similar situation: we have two pairs, it records the first pair, but not the other one. 
We can do hands with one group of cards, but not hands with 2. We need a way to keep track of at least 
two different groups of cards, tracking the number of cards and the rank of each. 
Think about it a bit before moving on.
Note: this is definitely the most intense logic of the program, so if you don't get this at first, don't worry. 
The rest of the code is all a little easier, and you can come back to this part later. Smile | :)

My solution: all right, we have to keep track of two ranks of cards and how many cards are of each rank, 
so we'll have two variables to represent the ranks (largeGroupRank, smallGroupRank) and two to represent 
the number of cards that have that rank (sameCards, sameCards2).
*/
if (dsmap_suits[? "hearts"] == 5 or dsmap_suits[? "clubs"] == 5 or 
    dsmap_suits[? "diamonds"] == 5 or dsmap_suits[? "spades"] == 5) 
    { 
    var flush = true; 
    } 
else 
    {
    var flush = false; 
    }

/*To figure out if there's a straight, we need to know if there are five cards in a row. 
So, if there is one card in five5 consecutive ranks, we have a straight.
*/
var topStraightValue=0;
var straight=false;  //assume no straight  
for (var i=1; i<=9; i++) //can't have straight with lowest value of more than 10
    {
    if (ranks[i]==1 and 
        ranks[i+1]==1 and 
        ranks[i+2]==1 and
        ranks[i+3]==1 and 
        ranks[i+4]==1) 
        {
            straight=true;
            topStraightValue=i+4; //4 above bottom value
            break;
        } 
    }

// check straight à l'as
if (ranks[10]==1 and ranks[11]==1 and ranks[12]==1 and ranks[13]==1 and ranks[1]==1) 
    {
    straight=true;
    topStraightValue=14; //higher than king
    }

/*We check to see if there is one card of 5 consecutive ranks. There's a loop to do straights up to 
king high, and we add a special separate if for an ace high straight, 
since the number of aces is contained in ranks[1].
*/
var royal_flush = false;
if (ranks[10]==1 and 
    ranks[11]==1 and
    ranks[12]==1 and
    ranks[13]==1 and 
    ranks[1]==1) 
        and 
    (dsmap_suits[? "clubs"] == 5 or dsmap_suits[? "diamonds"] == 5 or 
        dsmap_suits[? "spades"] == 5 or dsmap_suits[? "hearts"] == 5)  //ace high {
        {
        topStraightValue=14; //higher than king
        royal_flush = true;
        }

/*
Yay, we've covered all the different types of hands! Now, we need to start comparing them. 
We have what we need to determine the type of the hand, but we still need a little more data to fix 
ties between hands. Say we have a pair, we know that a pair is the second lowest ranked hand. 
If the hand we're comparing it to is also a pair, then we need to compare the rank of the pair. 
If the rank of the pair is equal, we need to go to the next highest card, then the next highest card, 
and then the next highest card. The only thing we need now is the next highest cards in order.
*/
var orderedRanks = 0;
orderedRanks[5] = 0;
var index=0;
if (ranks[1]==1) {//if ace, run this before because ace is highest card
    //record an ace as 14 instead of one, as its the highest card
    orderedRanks[index]=14;
    index++; //increment position
}

for (var i=13; i>=2; i--) {
    if (floor(ranks[i])==1) {
    //we have already written code to handle the case
    //of their being two cards of the same rank
        orderedRanks[index]=i; 
        index++;
    }
}

// -------------------------------------------------------------------------------------------------------
// STARTING EVALUATION
// -------------------------------------------------------------------------------------------------------
// JACKS OR BETTER 
if ( floor(sameCards)==2 and floor(sameCards2)==1 and (floor(ranks[11]) == 2 or floor(ranks[12]) == 2 or 
    floor(ranks[13]) == 2 or floor(ranks[1] == 2)) ) {
    scr_scoreboard_blink(txt_jacks_or_better);
    with (txt_hand_result) { uiTextValue = 'Jacks or Better'; }
    if (global.is_draw){
        global.win = global.bet * 1;
        global.credits += global.bet;
        play_win_sound(); 
    }
}

// -------------------------------------------------------------------------------------------------------
// TWO PAIR 
if (sameCards == 2 && sameCards2== 2) {//two pair
    with (txt_hand_result) { uiTextValue = 'Two Pair'; }
    scr_scoreboard_blink(txt_two_pairs);
    if (global.is_draw) {
        global.win = global.bet * 2;
        global.credits += global.win;
        play_win_sound(); 
    }
}

// -------------------------------------------------------------------------------------------------------
// THREE OF A KIND NOT FULL HOUSE 
if (sameCards==3 && sameCards2!=2) {
    with (txt_hand_result) uiTextValue = 'Three of a Kind';
    scr_scoreboard_blink(txt_three_of_a_kind);
    if (global.is_draw) {
        global.win = global.bet * 3;
        global.credits += global.win;
        play_win_sound();         
    }
}

// -------------------------------------------------------------------------------------------------------
// STRAIGHT
if (straight and !flush and !royal_flush) {
    scr_scoreboard_blink(txt_straight);
    with (txt_hand_result) uiTextValue = 'Straight';
    if (global.is_draw) {
        global.win = global.bet * 4;
        global.credits += global.win;
        with (lbl_bet) uiSetScore = global.win;
        play_win_sound();
    }
}

// -------------------------------------------------------------------------------------------------------
// FLUSH 
if (flush and !straight and !royal_flush) {
     scr_scoreboard_blink(txt_flush);
     with (txt_hand_result) uiTextValue = 'Flush';
     if (global.is_draw) {
        global.win = global.bet * 6;
        global.credits += global.win;
        play_win_sound();
     }
}

// -------------------------------------------------------------------------------------------------------
// STRAIGHT FLUSH 
if (straight and flush and !royal_flush) { //straight flush
    scr_scoreboard_blink(txt_straight_flush);
    with (txt_hand_result) uiTextValue = 'Straight Flush';
    if (global.is_draw) {
        global.win = global.bet * 50;
        global.credits += global.win;
        play_win_sound(); 
    }
}

// -------------------------------------------------------------------------------------------------------
// ROYAL FLUSH
if royal_flush {
    scr_scoreboard_blink(txt_natural_royal);
    with (txt_hand_result) uiTextValue = 'Royal Flush';
    if (global.is_draw) {
        if global.bet < 5 global.win = global.bet * 250;
        else global.win = 4000;
        global.credits += global.win;
        play_win_sound();              
    }
}

// -------------------------------------------------------------------------------------------------------
// FULL HOUSE
if (sameCards==3 && sameCards2==2) {
    scr_scoreboard_blink(txt_full_house);
    with (txt_hand_result) uiTextValue = 'Full House';
    if (global.is_draw) {
        global.win = global.bet * 9;
        global.credits += global.win;
        play_win_sound();         
    }
}

// -------------------------------------------------------------------------------------------------------
// FOUR OF A KIND
if (sameCards==4){
    scr_scoreboard_blink(txt_four_of_a_kind);
    with (txt_hand_result) uiTextValue = 'Four of a kind';
    if (global.is_draw) {
        global.win = global.bet * 25;
        global.credits += global.win;
        play_win_sound();         
    }
}

// Adds our winnning to the global value
ini_open( INI_FILE_NAME );
global.totalWins += global.win;
if (global.is_draw and global.win == 0 and global.sound and !audio_is_playing(loser_snd)) { audio_play_sound(loser_snd,1,false); }
ini_write_real("general","totalwins",global.totalWins);
ini_close();

// Saves the game
save_game();

// frees memory
ranks = 0;
ds_map_destroy(dsmap_suits);
