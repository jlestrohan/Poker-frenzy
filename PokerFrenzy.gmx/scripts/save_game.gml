//#define save_game
/// save_game()

/*
    saves the game locking the state to avoid reload room and force program to go thru the first screen
*/

// 1) on crée une ds_map a partir de nos globales
var dsmap_allGlobals = ds_map_create(); 

ds_map_add(dsmap_allGlobals, "credits", global.credits);
ds_map_add(dsmap_allGlobals, "totalwins", global.totalWins);
ds_map_add(dsmap_allGlobals, "premium", global.premium);
ds_map_add(dsmap_allGlobals, "launches", global.launches);

ds_map_secure_save(dsmap_allGlobals, "g_data.dat");

// 4) libere la memoire de cette ds_map qui ne sert plus a que dalle
ds_map_destroy(dsmap_allGlobals);
