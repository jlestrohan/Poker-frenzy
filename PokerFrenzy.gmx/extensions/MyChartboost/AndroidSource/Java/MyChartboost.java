package ${YYAndroidPackageName};

import com.chartboost.sdk.CBLocation;
import com.chartboost.sdk.Chartboost;
import com.yoyogames.runner.RunnerJNILib;
import com.ginealgames.chartboostgms.*;

import ${YYAndroidPackageName}.R;
import ${YYAndroidPackageName}.RunnerActivity;

public class MyChartboost {
	
	private ChartboostGMS cbGMS=null;
	
	public void Init(String appId,String appSignature) {
		if (cbGMS==null) {
			cbGMS=new ChartboostGMS(RunnerActivity.CurrentActivity, RunnerActivity.ViewHandler, new ChartboostGMSInterface(){

				@Override
				public void ReturnAsync(String event, double val) {
					// TODO Auto-generated method stub
					int dsMapIndex=RunnerJNILib.jCreateDsMap(null, null, null);
					RunnerJNILib.DsMapAddString( dsMapIndex, "type", event );
					RunnerJNILib.DsMapAddDouble( dsMapIndex, "value", val);
					RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, 70);
				}
				
			});
		}
		cbGMS.Init(appId, appSignature);
	}
	
	public void showInterstitial(){
		cbGMS.showInterstitial();
	}
	
	public void showMoreApps() {
		cbGMS.showMoreApps();
	}
	
	public void showRewardedVideo() {
		cbGMS.showRewardedVideo();
	}
	
	public void cacheInterstitial() {
		cbGMS.cacheInterstitial();
	}
	
	public void cacheMoreApps() {
		cbGMS.cacheMoreApps();
	}
	
	public void cacheRewardedVideo() {
		cbGMS.cacheRewardedVideo();
	}

}
