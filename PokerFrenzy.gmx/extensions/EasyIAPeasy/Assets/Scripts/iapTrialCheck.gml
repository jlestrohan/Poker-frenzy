///iapTrialCheck()

/*
    Don't allow IAPs in trial mode - that's just naughty...
*/

if ((os_type == os_win8native) && win8_license_trial_version())   { show_message_async("IAPs are disabled in trial mode."); return true; }
if ((os_type == os_winphone) && winphone_license_trial_version()) { show_message_async("IAPs are disabled in trial mode."); return true; }

// We're all good
return false;