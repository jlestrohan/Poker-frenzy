///iapBuy()

/*
    This script deals with actually making a purchase.
    
    It confirms you don't already own a durable product before attempting to buy it
*/

if (iapTrialCheck()) { exit; }

// Not in trial mode? Okay, on we go...

var product = argument0;
var purchased = false;

if (iap_status() == iap_status_available)
{
    if (os_type == os_macosx) { product = product + "_mac"; }
    
    purchased = global.saveData[? product]; // Have we already bought this IAP?

    if (purchased) // Yes, we have
    {
        // Check if we still need to consume
        if (file_exists("productList.txt"))
        {
            var inputFile = file_text_open_read("productList.txt");
            
            file_text_readln(inputFile); // Skip the company name line
            file_text_readln(inputFile); // Skip the adsOrNot line
            var tempString = "";
            var tempReal = -1;
            
            while (!file_text_eof(inputFile)) // Read in the rest of the file
            {  
                tempString = file_text_read_string(inputFile);
                file_text_readln(inputFile);
                
                if (tempString == product) // If we've found a match for the product we're looking for
                {
                    tempReal = file_text_read_real(inputFile); // See if the product is durable or not
                    break; // Now stop reading the rest of the text file
                }
                else { file_text_readln(inputFile); } // Skip the consumable/durable line for this unwanted product
            }
            
            file_text_close(inputFile);
                
            if (tempReal == 0) // It IS consumable, so ensure it is consumed and they can buy again
            {
                if (global.showDebug) { show_debug_message("Consuming " + product); }
                iap_consume(product);
                global.saveData[? product] = false;
            }
            else { show_message_async("You already own this IAP."); } // Don't attempt to consume durable IAPs!
        }
    } 
    else
    {
        if (global.showDebug) { show_message_async("Buying " + product); }
        iap_acquire(product, "");
    }
}
else { show_message_async("Store is not available. Please check your network connection."); }